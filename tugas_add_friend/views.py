from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.test import Client
from django.urls import resolve
from .forms import Message_Form
from .models import Message
from urllib.request import urlopen
import urllib.error

# Create your views here.
response = {'author' : 'Samuel'}
def index(request):
    moedel = Message.objects.all()
    response['form'] = Message_Form
    response['model'] = moedel
    response['title'] = 'Add Friend'
    html = 'addfriend.html'
    return render(request, html, response)

def message_post(request):
    form = Message_Form(request.POST or None)
    moedel = Message.objects.all()
    if(request.method == 'POST' and form.is_valid()):
        if (cek_link(request.POST['link']) == True):
            response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
            response['link'] = request.POST['link']
            moedel = Message(name=response['name'], link=response['link'])
            moedel.save()
            response['status'] = "Done"
            return HttpResponseRedirect('/tugas-add-friend/')
        else:
            response['status'] = "Fail"
            return HttpResponseRedirect('/tugas-add-friend/')
    else:
        response['status'] = "Fail"
        return HttpResponseRedirect('/tugas-add-friend/')

def delete_friend(request, id_friend):
    friend = Message.objects.get(pk = id_friend)
    friend.delete()
    return HttpResponseRedirect('/tugas-add-friend/')

def cek_link(url):
    if ("herokuapp" in url):
        try:
            html = urlopen(url)
            return True
        except urllib.error.HTTPError as err:
            return False
    else:
        return False
