from django.apps import AppConfig


class TugasAddFriendConfig(AppConfig):
    name = 'tugas_add_friend'
