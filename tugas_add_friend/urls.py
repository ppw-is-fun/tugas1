from django.conf.urls import url
from .views import index, message_post,delete_friend
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^message_post', message_post, name='message_post'),
    #url(r'^delete_friend', delete_friend, name='delete_friend')
    url(r'^delete_friend/(?P<id_friend>\d+)/$', delete_friend, name = 'delete_friend'),
    #url(r"^delete_friend", delete_friend, name="delete_friend"),


]
