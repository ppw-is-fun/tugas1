from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, delete_friend
from .models import Message
from .forms import Message_Form
# Create your tests here.

class AddFriendTest(TestCase):

    #def Add_Friend_Url_is_exist(self):
        #response = Client().get('/tugas-add-friend/')
        #self.assertEqual(response.status_code, 200)
	
    def test_Add_Friend_using_index_func(self):
        found = resolve('/tugas-add-friend/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_friend(self):
            # Creating a new activity
        new_activity = Message.objects.create(name='samuel gansss', link='http://samuelppw.herokuapp.com')

            # Retrieving all available activity
        counting_all_available_todo = Message.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_form_validation_for_blank_items(self):
        form = Message_Form(data={'name': '', 'link': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['link'],
            ["This field is required."])

    def test_Add_Friend_post_fail(self):
        response = Client().post('/tugas-add-friend/message_post', {'name': 'Anonymous', 'link': 'A'})
        self.assertEqual(response.status_code, 302)

    def test_Add_Friend_post_success_and_render_the_result(self):
        anonymous = 'Anonymous'
        link = 'http://samuelppw.herokuapp.com'
        response_post = Client().post('/tugas-add-friend/message_post', {'name': '', 'link': 'http://samuelppw.herokuapp.com'})
        self.assertEqual(response_post.status_code, 302)
        response = Client().get('/tugas-add-friend/')
        html_response = response.content.decode('utf8')
        self.assertIn(anonymous,html_response)
        self.assertIn(link,html_response)

    def test_delete_Add_Friend_Function(self):
        moedel = Message.objects.create(name='Ganteng', link='http://samuelppw.herokuapp.com')
        entry = Message.objects.get(name='Ganteng')
        response = delete_friend(entry,entry.id)
        self.assertEqual(response.status_code, 302)

    def test_link_not_found(self):
        response = Client().post('/tugas-add-friend/message_post', {'name': 'Anonymous', 'link': 'http://kodokijo123.herokuapp.com/'})
        counting_all_available_todo = Message.objects.all().count()
        self.assertEqual(counting_all_available_todo, 0)

    def test_link_not_heroku(self):
        response = Client().post('/tugas-add-friend/message_post', {'name': 'Anonymous', 'link': 'http://google.com'})
        counting_all_available_todo = Message.objects.all().count()
        self.assertEqual(counting_all_available_todo, 0)
