from django.apps import AppConfig


class TugasStatsConfig(AppConfig):
    name = 'tugas_stats'
