from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from tugas_add_friend.models import Message

# Create your tests here.
class StatsTest(TestCase):
	
    def test_Stats_using_index_func(self):
        found = resolve('/tugas-stats/')
        self.assertEqual(found.func, index)

    def test_Stats_friend_render(self):
        response = Client().get('/tugas-stats/')		
        Data_friend = Message(name="Ganteng", link="http://www.ganteng.herokuapp.com")
        Data_friend.save()
        self.assertEqual(Message.objects.all().count(), 1)
        response_post = Client().post('/tugas-update-status/add_status', {'description': "Ganteng"})
        html_response = response.content.decode('utf8')
        self.assertIn("1", html_response)
        self.assertIn("Kak",html_response)
        self.assertEqual(response_post.status_code, 302)
        response1= Client().get('/tugas-stats/')
        html_response1 = response1.content.decode('utf8')
        self.assertIn("Ganteng", html_response1)