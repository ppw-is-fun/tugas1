from django.shortcuts import render
from tugas_halaman_profile.views import UserProfile
from tugas_update_status.models import Status
from tugas_add_friend.models import Message
# Create your views hereself.
response = {}
def index(request):
    profile_tampilan = UserProfile(name="Kak Pewe", position="Teaching Assistant at University of Indonesia",
                                   birthday="1998-05-12", expertise="", description="", email="", gender="Male", 
                                   link="https://cdn1.iconfinder.com/data/icons/user-pictures/100/male3-512.png")
    profile_tampilan.save()
    profile = UserProfile.objects.get(pk=1)
    response= {'title':'Stats', 'profile': profile,'friends':len(Message.objects.all()),'feeds':len(Status.objects.all())}
    response['latest'] = Status.objects.last()
    return render(request, 'stats.html', response)