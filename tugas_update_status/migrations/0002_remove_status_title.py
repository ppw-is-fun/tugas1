# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-09 09:42
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tugas_update_status', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='status',
            name='title',
        ),
    ]
