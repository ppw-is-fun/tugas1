from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, add_status, delete_status
from .models import Status
from .forms import Status_Form


# Create your tests here.
class Lab5UnitTest(TestCase):

    def test_lab_5_url_is_exist(self):
        response = Client().get('/tugas-update-status/')
        self.assertEqual(response.status_code, 200)

    def test_lab5_using_index_func(self):
        found = resolve('/tugas-update-status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_activity = Status.objects.create(description='mengerjakan tugas ppw')

        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_status_input_has_placeholder_and_css_classes(self):
        form = Status_Form()
        self.assertIn('class="status-form-textarea', form.as_p())
        self.assertIn('id="id_description', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )
    def test_lab5_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/tugas-update-status/add_status', {'description': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/tugas-update-status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_lab5_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/tugas-update-status/add_status', {'description': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/tugas-update-status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_delete_status(self):
        test_delete = Status.objects.create(description='test fungsi')
        request = HttpRequest()
        id = test_delete.id
        response = delete_status(request, id)
        self.assertEqual(response.status_code, 302)
