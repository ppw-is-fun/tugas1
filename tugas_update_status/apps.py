from django.apps import AppConfig


class TugasUpdateStatusConfig(AppConfig):
    name = 'tugas_update_status'
