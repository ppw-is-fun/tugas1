from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status

# Create your views here.
response = {}
def index(request):
    response['author'] = "NHR" #TODO Implement yourname
    status = Status.objects.all()
    response['form'] = Status_Form
    response['model'] = status
    response['title'] = 'Update Status'
    html = 'tugas_update_status.html'
    response['status_form'] = Status_Form
    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        status = Status(description=response['description'])
        status.save()
        #html ='lab_4/form_result.html'
        return HttpResponseRedirect('/tugas-update-status/')
    else:
        return HttpResponseRedirect('/tugas-update-status/')

def delete_status(request, id_status):
    status = Status.objects.get(pk = id_status)
    status.delete()
    return HttpResponseRedirect('/tugas-update-status/')
