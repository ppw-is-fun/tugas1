"""tugas1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.views.generic.base import RedirectView
from django.contrib import admin
import tugas_update_status.urls as tugas_update_status
import tugas_halaman_profile.urls as tugas_halaman_profile
import tugas_add_friend.urls as tugas_add_friend
import tugas_stats.urls as tugas_stats

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^tugas-update-status/', include(tugas_update_status,namespace='tugas-update-status')),
    url(r'^tugas-halaman-profile/', include(tugas_halaman_profile,namespace='tugas-halaman-profile')),
    url(r'^tugas-add-friend/', include(tugas_add_friend, namespace='tugas-add-friend')),
     url(r'^tugas-stats/', include(tugas_stats, namespace='tugas-stats')),
    url(r'^$', RedirectView.as_view(permanent = True, url = '/tugas-update-status/'), name='index')
]
