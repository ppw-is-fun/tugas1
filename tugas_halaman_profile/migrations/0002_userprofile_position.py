# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-10 17:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tugas_halaman_profile', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='position',
            field=models.CharField(default='Teaching Assistant at University of Indonesia', max_length=250),
        ),
    ]
