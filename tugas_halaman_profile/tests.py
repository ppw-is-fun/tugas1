from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import UserProfile

class TugasProfilePageTest(TestCase):

    def test_Profile_Page_Using_Index_func(self):
        found = resolve('/tugas-halaman-profile/')
        self.assertEqual(found.func, index)

    def test_Profile_Page_Lengkap(self):
        response = Client().get('/tugas-halaman-profile/')
        html_response = response.content.decode('utf8')
        self.assertIn("Kak",html_response)
        self.assertIn("Teaching Assistant at University of Indonesia",html_response)
        self.assertIn("Male",html_response)
        self.assertIn("https://cdn1.iconfinder.com/data/icons/user-pictures/100/male3-512.png",html_response)
        counting_all_available_todo = UserProfile.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)