from django.db import models

# Create your models here.

class UserProfile(models.Model):
    name = models.CharField(max_length=250, default="Kak Pewe")
    position = models.CharField(max_length=250, default="Teaching Assistant at University of Indonesia")
    birthday = models.DateField()
    gender = models.CharField(max_length=10, default="Male")
    expertise = models.CharField(max_length=250)
    description = models.TextField()
    email = models.EmailField()
    link = models.TextField(default="https://cdn1.iconfinder.com/data/icons/user-pictures/100/male3-512.png")
