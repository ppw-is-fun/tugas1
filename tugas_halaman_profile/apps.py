from django.apps import AppConfig


class TugasHalamanProfileConfig(AppConfig):
    name = 'tugas_halaman_profile'
