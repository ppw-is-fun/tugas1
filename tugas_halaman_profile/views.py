from django.shortcuts import render
from .models import UserProfile
# Create your views here.


def index(request):
    profile_tampilan = UserProfile(name="Kak Pewe", position="Teaching Assistant at University of Indonesia",birthday="1998-05-12",
                                   expertise="", description="", email="", gender="Male",
                                   link="https://cdn1.iconfinder.com/data/icons/user-pictures/100/male3-512.png")
    profile_tampilan.save()
    profile = UserProfile.objects.get(pk=1)
    splitexp = profile.expertise.split(',')
    response= {'title':'Profile', 'profile': profile, 'splitexp':splitexp}
    return render(request, 'profile.html', response)